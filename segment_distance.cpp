#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>

struct Point
{
    long double x;
    long double y;
    long double z;

    Point(long double x1, long double y1, long double z1):x(x1), y(y1), z(z1)
    {}
    Point()
    {}
};
Point operator-(const Point& a, const Point& b)
{
    Point c = Point(a.x - b.x, a.y - b.y, a.z - b.z);
    return c;
}
Point operator+(const Point& a, const Point& b)
{
    Point c = Point(a.x + b.x, a.y + b.y, a.z + b.z);
    return c;
}
long double len(const Point& c)
{
    return sqrt(c.x * c.x + c.y * c.y + c.z * c.z);
}
Point operator*(const Point& a, const Point& b)
{
    Point c = Point(a.y * b.z - b.y * a.z, - a.x * b.z + b.x * a.z, a.x * b.y - a.y * b.x);
    return c;
}
Point operator*(long double q, const Point& a)
{
    return Point(q * a.x, q * a.y, q * a.z);
}
long double ternary_search2(const Point& a, const Point& b, const Point& c, const long double& eps)
{
    Point p_left, p_right;
    long double left = 0.0, right = 1.0;
    long double t_for_left, t_for_right;
    while(right - left >= eps)
    {
        t_for_left = left + (right - left)/3;
        t_for_right = left + 2*(right - left)/3;
        p_left = b + t_for_left*(c - b);
        p_right = b + t_for_right*(c - b);
        if(len(a - p_left) < len(a - p_right))
        {
            right = t_for_right;
        }
        else
        {
            left = t_for_left;
        }
    }
    t_for_left = (left + right)/2;
    p_left = b + t_for_left*(c - b);
    return len(a - p_left);
}
long double ternary_search(const Point& a, const Point& b, const Point& c, const Point& d, const long double& eps)
{
    long double left = 0.0;
    long double right = 1.0;
    while(right - left >= eps)
    {
        Point p_left = a + (left + (right - left)/3) * (b - a);
        Point p_right = a + (left + 2*(right - left)/3) * (b - a);
        long double ans1 = ternary_search2(p_left, c, d, eps);
        long double ans2 = ternary_search2(p_right, c, d, eps);
        if(ans1 < ans2)
        {
            right =  left + 2*(right - left)/3;
        }
        else
        {
            left =  left + (right - left)/3;
        }
    }
    Point ans_p = a + ((left + right)/2)*(b - a);
    return ternary_search2(ans_p, c, d, eps);
}
int main()
{
    Point a, b, c, d;
    const long double eps = 1e-9;
    std::cout.precision(12);
    std::cin >> a.x >> a.y >> a.z >> b.x >> b.y >> b.z;
    std::cin >> c.x >> c.y >> c.z >> d.x >> d.y >> d.z;
    std::cout << std::fixed << ternary_search(a, b, c, d, eps);
    return 0;
}
