#include <iostream>
#include <string>
#include <vector>

std::vector<size_t> prefix_to_z(std::vector<size_t>& prefix)
{
    std::vector<size_t> z(prefix.size());
    for(size_t i = 0; i < prefix.size(); ++i)
    {
        if(prefix[i] > 0)
        {
            size_t lost_prefix;
            lost_prefix = prefix[i];
            while(lost_prefix > 0)
            {
                z[i - lost_prefix + 1] = lost_prefix;
                lost_prefix = prefix[lost_prefix - 1];
            }
        }
    }
    return z;
}
int main()
{
    std::vector<size_t> prefix;
    size_t current_prefix;
    while(std::cin >> current_prefix)
    {
        prefix.push_back(current_prefix);
    }
    std::vector<size_t> z;
    z = prefix_to_z(prefix);
    for(size_t cur_z : z)
    {
        std::cout << cur_z << " ";
    }
    return 0;
}
