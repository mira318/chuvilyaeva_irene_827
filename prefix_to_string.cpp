#include <cstring>
#include <iostream>
#include <string>
#include <vector>

char count_next_letter(std::vector<size_t>& prefix, size_t current_place, std::string& ans_string)
{
    bool bad_letter[26];
    if(prefix[current_place] > 0)
    {
        return ans_string[prefix[current_place] - 1];
    }
    else
    {
        std::memset(bad_letter, false, sizeof bad_letter);
        size_t previous_prefix = prefix[current_place - 1];
        while(previous_prefix > 0)
        {
            bad_letter[ans_string[previous_prefix] - 'a'] = true;
            previous_prefix = prefix[previous_prefix - 1];
        }
        size_t j = 0;
        bad_letter[0] = true;
        while(bad_letter[j])
        {
            j++;
        }
        return 'a' + j;
    }
}
int main()
{
    std::vector<size_t> prefix;
    size_t i;
    size_t current_prefix;
    std::string ans_string;
    ans_string.push_back('a');
    i = 0;
    while(std::cin >> current_prefix)
    {
        prefix.push_back(current_prefix);
        if(i > 0)
        {
            ans_string.push_back(count_next_letter(prefix, i, ans_string));
        }
        i++;
    }
    std::cout << ans_string;
    return 0;
}
