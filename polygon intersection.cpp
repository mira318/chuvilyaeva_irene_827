#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

class Point
{
    public:
        long double x;
        long double y;

        Point(long double x1, long double y1):x(x1), y(y1)
        {}
        Point()
        {}
        long double len()
        {
            return sqrt(x * x + y * y);
        }
        Point operator-(const Point& a)
        {
            Point c = Point(x - a.x, y - a.y);
            return c;
        }
        Point operator+(const Point& a)
        {
            Point c = Point(x + a.x, y + a.y);
            return c;
        }
};

long double mix_product(const Point& a, const Point& b, const Point& c)
{
    return a.x * (b.y - c.y) - a.y * (b.x - c.x) + (b.x * c.y - c.x * b.y);
}
Point operator*(long double q, const Point& a)
{
    return Point(q * a.x, q * a.y);
}
bool operator<(const Point& a, const Point& b)
{
    return a.x < b.x  || (a.x == b.x && a.y < b.y);
}
std::vector<Point> right_order(const std::vector<Point>& figure)
{
    std::vector<Point> ans(figure.size());
    int min_index = -1;
    for(int i = 0; i < figure.size(); ++i)
    {
        if(min_index == -1 || figure[i] < figure[min_index])
        {
            min_index = i;
        }
    }
    int cur = min_index;
    for(int i = 0; i < figure.size(); ++i)
    {
        ans[i] = figure[cur];
        cur--;
        if(cur < 0)
        {
            cur += figure.size();
        }
    }
    return ans;
}
bool check_zero(const std::vector<Point>& figure)
{
    Point zero = Point(0, 0);
    for(int i = 0; i < figure.size() - 1; ++i)
    {
        if(mix_product(zero, figure[i], figure[i + 1]) <= 0)
        {
            return false;
        }
    }
    return true;
}
std::vector<Point> count_sum(std::vector<Point>& figure1, int n, std::vector<Point>& figure2, int m)
{
    figure1 = right_order(figure1);
    figure2 = right_order(figure2);
    figure1.push_back(figure1[0]);
    figure2.push_back(figure2[0]);
    std::vector<Point> ans_figure;
    ans_figure.push_back(figure1[0] + figure2[0]);
    int ind1 = 0;
    int ind2 = 0;
    while(ind1 < n && ind2 < m)
    {
        Point from_first, from_second;
        if(ind1 < n)
        {
            from_first = ans_figure[ans_figure.size() - 1] + figure1[ind1 + 1] - figure1[ind1];
            if(ind2 < m)
            {
                from_second = ans_figure[ans_figure.size() - 1] + figure2[ind2 + 1] - figure2[ind2];
                if(mix_product(ans_figure[ans_figure.size() - 1], from_first, from_second) == 0)
                {
                    if((from_first - ans_figure[ans_figure.size() - 1]).len() > (from_second - ans_figure[ans_figure.size() - 1]).len())
                    {
                        ans_figure.push_back(from_first);
                    }
                    else
                    {
                        ans_figure.push_back(from_second);
                    }
                    ind1++;
                    ind2++;
                }
                else
                {
                    if(mix_product(ans_figure[ans_figure.size() - 1], from_first, from_second) > 0)
                    {
                        ans_figure.push_back(from_first);
                        ind1++;
                    }
                    else
                    {
                        ans_figure.push_back(from_second);
                        ind2++;
                    }
                }
            }
            else
            {
                ans_figure.push_back(from_first);
                ind1++;
            }
        }
        else
        {
            from_second = figure1[ind1 + 1] + figure2[ind2];
            ans_figure.push_back(from_second);
            ind2++;
        }
    }
    return ans_figure;
}
int main()
{
    int n, m;
    std::cin >> n;
    std::vector<Point> figure1(n);
    for(int i = 0; i < n; ++i)
    {
        std::cin >> figure1[i].x >> figure1[i].y;
    }
    std::cin >> m;
    std::vector<Point> figure2(m);
    for(int i = 0; i < m; ++i)
    {
        std::cin >> figure2[i].x >> figure2[i].y;
        figure2[i] = -1*figure2[i];
    }
    std::vector<Point> figure3;
    figure3 = count_sum(figure1, n, figure2, m);
    if(check_zero(figure3))
    {
        std::cout << "YES";
    }
    else
    {
        std::cout << "NO";
    }
    return 0;
}