#include <cstring>
#include <iostream>
#include <vector>
#include <string>

void prefix_to_string(std::vector<int>& prefix, std::string& ans_string)
{
    bool bad_letter[26];
    int current_prefix;
    ans_string.push_back('a');
    for(size_t i = 0; i < prefix.size(); ++i)
    {
        if(i > 0)
        {
            if(prefix[i] > 0)
            {
                ans_string.push_back(ans_string[prefix[i] - 1]);
            }
            else
            {
                std::memset(bad_letter, false, sizeof bad_letter);
                int previous_prefix = prefix[i - 1];
                while(previous_prefix > 0)
                {
                    bad_letter[ans_string[previous_prefix] - 'a'] = true;
                    previous_prefix = prefix[previous_prefix - 1];
                }
                int j = 0;
                bad_letter[0] = true;
                while(bad_letter[j])
                {
                    j++;
                }
                ans_string.push_back('a' + j);
            }
        }
    }
}
void z_to_prefix(std::vector<int>& z, std::vector<int>& prefix)
{
    for(size_t i = 1; i < z.size(); ++i)
    {
        if(z[i] > 0)
        {
            int j = i + z[i] - 1;
            int current_ans = z[i];
            while(current_ans > 0 && prefix[j] == 0)
            {
                prefix[j] = current_ans;
                j--;
                current_ans--;

            }
        }
    }
}
int main()
{
    std::vector<int> z;
    int current_z;
    while(std::cin >> current_z)
    {
        z.push_back(current_z);
    }
    std::vector<int> prefix(z.size(), 0);
    z_to_prefix(z, prefix);
    std::string ans_string;
    prefix_to_string(prefix, ans_string);
    std::cout << ans_string;
    return 0;
}
