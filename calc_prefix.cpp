#include <iostream>
#include <string>
#include <vector>

std::vector<size_t> CalcPrefix(std::string& S)
{
    std::vector<size_t> Prefix(S.size());
    Prefix[0] = 0;
    size_t ans;
    for(size_t i = 1; i < S.size(); ++i)
    {
        ans = Prefix[i - 1];
        while(ans > 0 && S[i] != S[ans])
        {
            ans = Prefix[ans - 1];
        }
        if(S[i] == S[ans])
            ans++;
        Prefix[i] = ans;
    }
    return Prefix;
}
int main()
{
    std::string Text;
    std::cin >> Text;
    std::vector<size_t> Prefix_for_text = CalcPrefix(Text);
    for(size_t i = 0; i < Text.size(); ++i)
    {
        std::cout << Prefix_for_text[i] << " ";
    }
    return 0;
}
