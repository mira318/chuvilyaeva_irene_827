#include <iostream>
#include <vector>

std::vector<size_t> z_to_prefix(std::vector<size_t>& z)
{
    std::vector<size_t> prefix(z.size(), 0);
    for(size_t i = 1; i < z.size(); ++i)
    {
        if(z[i] > 0)
        {
            size_t j = i + z[i] - 1;
            size_t current_ans = z[i];
            while(current_ans > 0 && prefix[j] == 0)
            {
                prefix[j] = current_ans;
                j--;
                current_ans--;
            }
        }
    }
    return prefix;
}
int main()
{
    std::vector<size_t> z;
    size_t current_z;
    while(std::cin >> current_z)
    {
        z.push_back(current_z);
    }
    std::vector<size_t> prefix;
    prefix = z_to_prefix(z);
    for(size_t cur_pref : prefix)
    {
        std::cout << cur_pref << " ";
    }
    return 0;
}
