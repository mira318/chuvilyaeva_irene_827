#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

size_t stupid_move(std::string& s, size_t place_far, size_t place_close)
{
    size_t ans = 0;
    while(place_far < s.size() && s[place_far] == s[place_close])
    {
        place_far++;
        place_close++;
        ans++;
    }
    return ans;
}
std::vector<size_t> calc_z(std::string& s)
{
    std::vector<size_t> z;
    z.resize(s.size());
    z[0] = 0;
    size_t left = 0;
    size_t right = 0;
    size_t start;
    size_t i;
    for(i = 1; i < s.size(); ++i)
    {
        if(i > right)
        {
            start = 0;
        }
        else
        {
            start = std::min(z[i - left], right - i + 1);
        }
        z[i] = stupid_move(s, i + start, start) + start;
        if(i + z[i] - 1 > right)
        {
            right = i + z[i] - 1;
            left = i;
        }
    }
    return z;
}
int main()
{
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    std::string text;
    std::cin >> text;
    std::vector<size_t> z_for_text;
    z_for_text = calc_z(text);
    for(int i = 0; i < text.size(); ++i)
    {
        std::cout << z_for_text[i] << " ";
    }
    std::cout << std::endl;
    for(size_t cur_z : z_for_text)
    {
        std::cout << cur_z << " ";
    }
    return 0;
}
