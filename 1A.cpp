#include <iostream>
#include <string>
#include <vector>

size_t stupid_move(std::string& str, size_t place_far, size_t place_close)
{
    size_t ans = 0;
    while(place_far < str.size() && str[place_far] == str[place_close])
    {
        ++place_far;
        ++place_close;
        ++ans;
    }
    return ans;
}
std::vector<size_t> calc_z(std::string& str)
{
    std::vector<size_t>z(str.size());
    z[0] = 0;
    size_t left;
    size_t right;
    size_t start;
    size_t i;
    left = 0;
    right = 0;
    for(i = 1; i < str.size(); ++i)
    {
        if(i > right)
        {
            start = 0;
        }
        else
        {
            start = std::min(z[i - left], right - i + 1);
        }
        z[i] = start + stupid_move(str, start + i, start);
        if(right < z[i] + i - 1)
        {
            right = z[i] + i - 1;
            left = i;
        }
    }
    return z;
}
int main()
{
    std::vector<size_t> common_z;
    std::string pattern, text;
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    std::cin >> pattern >> text;
    std::string common = pattern + '\n' + text;
    common_z = calc_z(common);
    for(size_t i = pattern.size() + 1; i < common.size(); ++i)
    {
        if(common_z[i] == pattern.size())
        {
            std::cout << i - pattern.size() - 1 << " ";
        }
    }
    return 0;
}
