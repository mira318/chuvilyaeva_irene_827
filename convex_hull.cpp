#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <stack>
#include <vector>

struct Point
{
    long double x;
    long double y;
    long double z;
    Point* prev;
    Point* next;
    size_t num;
    Point(long double x1, long double y1, long double z1, size_t num1):x(x1), y(y1), z(z1), num(num1)
    {
        prev = NULL;
        next = NULL;
    }
    Point()
    {}
    bool is_insert()
    {
        if(prev->next == this)
        {
            prev->next = next;
            next->prev = prev;
            return false;
        }
        else
        {
            next->prev = this;
            prev->next = this;
            return true;
        }
    }
};
long double m_pr_2d(const Point* a, const Point* b, const Point* c)
{
    return a->x * (b->y - c->y) - a->y * (b->x - c->x) + ((b->x * c->y) - (c->x * b->y));
}
long double turn_time(const Point* a, const Point* b, const Point* c)
{
    return (a->x * (b->z - c->z) - a->z * (b->x - c->x) +
            ((b->x * c->z) - (c->x * b->z)))/ m_pr_2d(a, b, c);
}
bool operator<(const Point& a, const Point& b)
{
    if(a.x < b.x)
    {
        return true;
    }
    if(a.x == b.x && a.y < b.y)
    {
        return true;
    }
    if(a.x == b.x && a.y == b.y && a.z < b.z)
    {
        return true;
    }
    return false;
}
void perturbation(Point& a, long double ang)
{
    long double x, y, z;
    long double cos_ang = cos(ang);
    long double sin_ang = sin(ang);

    z = a.z * cos_ang + a.y * sin_ang;
    y = a.y * cos_ang - a.z * sin_ang;
    a.z = z;
    a.y = y;

    x = a.x * cos_ang + a.z * sin_ang;
    z = a.z * cos_ang - a.x * sin_ang;
    a.x = x;
    a.z = z;

    x = a.x * cos_ang + a.y * sin_ang;
    y = a.y * cos_ang - a.x * sin_ang;
    a.x = x;
    a.y = y;
}

struct Ordered_face
{
    size_t a, b, c;
    Ordered_face(size_t prev, size_t cur, size_t next, bool from_up, bool is_insert)
    {
        a = prev;
        b = cur;
        c = next;
        if(from_up && is_insert)
        {
            std::swap(a, b);
        }
        if(!from_up && !is_insert)
        {
            std::swap(a, b);
        }
        if(b < a && b < c)
        {
            std::swap(b, a);
            std::swap(b, c);
        }
        else
        {
            if(c < b && c < a)
            {
                std::swap(b, c);
                std::swap(b, a);
            }
        }
    }
    Ordered_face()
    {}
};
bool operator<(const Ordered_face& first, const Ordered_face& second)
{
    if(first.a < second.a)
    {
        return true;
    }
    if(first.a == second.a && first.b < second.b)
    {
        return true;
    }
    if(first.a == second.a && first.b == second.b && first.c < second.c)
    {
        return true;
    }
    return false;
}

std::vector<Point*> build_hull_down(std::vector<Point>& all, int left, int right, const long double& INF)
{
    std::vector<Point*> ans;
    if(right - left == 1)
    {
        return ans;
    }
    Point* u;
    Point* v;
    int mid = (left + right) / 2;
    std::vector<Point*> ans1;
    std::vector<Point*> ans2;
    ans1 = build_hull_down(all, left, mid, INF);
    ans2 = build_hull_down(all, mid, right, INF);
    u = &all[mid - 1];
    v = &all[mid];
    while(1)
    {
        if(v->next != NULL && v != NULL && u != NULL && m_pr_2d(u, v, v->next) < 0)
        {
            v = v->next;
        }
        else
        {
            if(u->prev != NULL && u != NULL && v != NULL && m_pr_2d(u->prev, u, v) < 0)
            {
                u = u->prev;
            }
            else
            {
                break;
            }
        }
    }
    Point* from_left;
    Point* from_right;
    size_t p_r = 0;
    size_t p_l = 0;
    long double change_time[6];
    long double now = -INF;
    long double next_card_time;
    int next_card_i;
    while(1)
    {
        from_left = NULL;
        from_right = NULL;
        if(p_l < ans1.size())
        {
            from_left = ans1[p_l];
        }
        if(from_left != NULL && from_left->prev != NULL && from_left->next != NULL)
        {
            change_time[0] = turn_time(from_left->prev, from_left, from_left->next);
        }
        else
        {
            change_time[0] = INF;
        }


        if(p_r < ans2.size())
        {
            from_right = ans2[p_r];
        }
        if(from_right != NULL && from_right->prev != NULL && from_right->next != NULL)
        {
            change_time[1] = turn_time(from_right->prev, from_right, from_right->next);
        }
        else
        {
            change_time[1] = INF;
        }


        if(u != NULL && u->next != NULL && v != NULL)
        {
            change_time[2] = turn_time(u, u->next, v);
        }
        else
        {
            change_time[2] = INF;
        }
        if(u->prev != NULL && u != NULL && v != NULL)
        {
            change_time[3] = turn_time(u->prev, u, v);
        }
        else
        {
            change_time[3] = INF;
        }


        if(u != NULL && v->prev != NULL && v != NULL)
        {
            change_time[4] = turn_time(u, v->prev, v);
        }
        else
        {
            change_time[4] = INF;
        }
        if(u != NULL && v != NULL && v->next != NULL)
        {
            change_time[5] = turn_time(u, v, v->next);
        }
        else
        {
            change_time[5] = INF;
        }


        next_card_time = INF;
        next_card_i = -1;
        for(size_t i = 0; i < 6; ++i)
        {
            if(change_time[i] < next_card_time && change_time[i] > now)
            {
                next_card_i = i;
                next_card_time = change_time[i];
            }
        }
        if(next_card_time == INF)
        {
            break;
        }
        switch (next_card_i)
        {
        case 0:
            p_l++;
            if(from_left->x < u->x)
            {
                ans.push_back(from_left);
            }
            from_left->is_insert();
            break;


        case 1:
            p_r++;
            if(from_right->x > v->x)
            {
                ans.push_back(from_right);
            }
            from_right->is_insert();
            break;


        case 2:
            u = u->next;
            ans.push_back(u);
            break;


        case 3:
            ans.push_back(u);
            u = u->prev;
            break;


        case 4:
            v = v->prev;
            ans.push_back(v);
            break;


        case 5:
            ans.push_back(v);
            v = v-> next;
            break;


        default:
            break;
        }
        now = next_card_time;
    }
    v->prev = u;
    u->next = v;
    for(int i = ans.size() - 1; i >= 0; --i)
    {
        if(ans[i]->x <= u->x || ans[i]->x >= v->x)
        {
            if(ans[i] == u)
            {
                u = u->prev;
            }
            if(ans[i] == v)
            {
                v = v->next;
            }
            ans[i]->is_insert();
        }
        else
        {
            v->prev = ans[i];
            u->next = ans[i];
            ans[i]->prev = u;
            ans[i]->next = v;
            if(ans[i]->x <= all[mid - 1].x)
            {
                u = ans[i];
            }
            else
            {
                v = ans[i];
            }
        }
    }
    return ans;
}


int main()
{
    const long double INF = 1e18;
    size_t q;
    std::cin >> q;
    for(size_t j = 0; j < q; ++j)
    {
        size_t n;
        std::cin >> n;
        std::vector<Point> all_point(n);
        for(size_t i = 0; i < n; ++i)
        {
            std::cin >> all_point[i].x >> all_point[i].y >> all_point[i].z;
            all_point[i].next = NULL;
            all_point[i].prev = NULL;
            perturbation(all_point[i], 0.0000001);
            all_point[i].num = i;
        }
        std::sort(all_point.begin(), all_point.end());
        std::vector<Point*> ans_d = build_hull_down(all_point, 0, n, INF);
        std::vector<Ordered_face> ans(ans_d.size());
        Ordered_face to_add;
        for(size_t i = 0; i < ans_d.size(); ++i)
        {
            to_add = Ordered_face(ans_d[i]->prev->num, ans_d[i]->num, ans_d[i]->next->num, false, ans_d[i]->is_insert());
            ans[i] = to_add;
        }
        for(size_t i = 0; i < n; ++i)
        {
            all_point[i].z = -all_point[i].z;
            all_point[i].prev = NULL;
            all_point[i].next = NULL;
        }
        std::vector<Point*> ans_u = build_hull_down(all_point, 0, n, INF);
        ans.resize(ans_u.size() + ans_d.size());
        for(size_t i = 0; i < ans_u.size(); ++i)
        {
            to_add = Ordered_face(ans_u[i]->prev->num, ans_u[i]->num, ans_u[i]->next->num, true, ans_u[i]->is_insert());
            ans[i + ans_d.size()] = to_add;
        }
        std::sort(ans.begin(), ans.end());
        std::cout << ans.size() << std::endl;
        for(size_t i = 0; i < ans.size(); ++i)
        {
            std::cout << "3 " << ans[i].a << " " << ans[i].b << " " << ans[i].c << std::endl;
        }
        std::cout << std::endl;
        all_point.clear();
        ans_d.clear();
        ans_u.clear();
        ans.clear();
    }
    return 0;
}
