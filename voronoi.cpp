#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <stack>
#include <vector>

struct Point
{
    long double x;
    long double y;
    long double z;
    Point* prev;
    Point* next;
    size_t num;
    Point(long double x1, long double y1, long double z1, size_t num1):x(x1), y(y1), z(z1), num(num1)
    {
        prev = NULL;
        next = NULL;
    }
    Point()
    {}
    bool is_insert()
    {
        if(prev->next == this)
        {
            prev->next = next;
            next->prev = prev;
            return false;
        }
        else
        {
            next->prev = this;
            prev->next = this;
            return true;
        }
    }
};
long double m_pr_2d(const Point* a, const Point* b, const Point* c)
{
    return a->x * (b->y - c->y) - a->y * (b->x - c->x) + ((b->x * c->y) - (c->x * b->y));
}
long double turn_time(const Point* a, const Point* b, const Point* c)
{
    return (a->x * (b->z - c->z) - a->z * (b->x - c->x) +
            ((b->x * c->z) - (c->x * b->z)))/ m_pr_2d(a, b, c);
}
bool operator<(const Point& a, const Point& b)
{
    if(a.x < b.x)
    {
        return true;
    }
    if(a.x == b.x && a.y < b.y)
    {
        return true;
    }
    if(a.x == b.x && a.y == b.y && a.z < b.z)
    {
        return true;
    }
    return false;
}
Point operator-(const Point& a, const Point& b)
{
    Point c = Point(a.x - b.x, a.y - b.y, 0, -1);
    return c;
}
long double vec_z(Point& a, Point& b)
{
    return a.x * b.y - a.y * b.x;
}

struct Ordered_face
{
    size_t a, b, c;
    Ordered_face(size_t prev, size_t cur, size_t next, bool from_up, bool is_insert)
    {
        a = prev;
        b = cur;
        c = next;
        if(from_up && is_insert)
        {
            std::swap(a, b);
        }
        if(!from_up && !is_insert)
        {
            std::swap(a, b);
        }
        if(b < a && b < c)
        {
            std::swap(b, a);
            std::swap(b, c);
        }
        else
        {
            if(c < b && c < a)
            {
                std::swap(b, c);
                std::swap(b, a);
            }
        }
    }
    Ordered_face()
    {}
};
bool operator<(const Ordered_face& first, const Ordered_face& second)
{
    if(first.a < second.a)
    {
        return true;
    }
    if(first.a == second.a && first.b < second.b)
    {
        return true;
    }
    if(first.a == second.a && first.b == second.b && first.c < second.c)
    {
        return true;
    }
    return false;
}
std::vector<Point*> build_hull_down(std::vector<Point>& all, int left, int right, const long double& INF)
{
    std::vector<Point*> ans;
    if(right - left == 1)
    {
        return ans;
    }
    Point* u;
    Point* v;
    int mid = (left + right) / 2;
    std::vector<Point*> ans1;
    std::vector<Point*> ans2;
    ans1 = build_hull_down(all, left, mid, INF);
    ans2 = build_hull_down(all, mid, right, INF);
    u = &all[mid - 1];
    v = &all[mid];
    while(1)
    {
        if(v->next != NULL && v != NULL && u != NULL && m_pr_2d(u, v, v->next) < 0)
        {
            v = v->next;
        }
        else
        {
            if(u->prev != NULL && u != NULL && v != NULL && m_pr_2d(u->prev, u, v) < 0)
            {
                u = u->prev;
            }
            else
            {
                break;
            }
        }
    }
    Point* from_left;
    Point* from_right;
    size_t p_r = 0;
    size_t p_l = 0;
    long double change_time[6];
    long double now = -INF;
    long double next_card_time;
    int next_card_i;
    while(1)
    {
        from_left = NULL;
        from_right = NULL;
        if(p_l < ans1.size())
        {
            from_left = ans1[p_l];
        }
        if(from_left != NULL && from_left->prev != NULL && from_left->next != NULL)
        {
            change_time[0] = turn_time(from_left->prev, from_left, from_left->next);
        }
        else
        {
            change_time[0] = INF;
        }


        if(p_r < ans2.size())
        {
            from_right = ans2[p_r];
        }
        if(from_right != NULL && from_right->prev != NULL && from_right->next != NULL)
        {
            change_time[1] = turn_time(from_right->prev, from_right, from_right->next);
        }
        else
        {
            change_time[1] = INF;
        }


        if(u != NULL && u->next != NULL && v != NULL)
        {
            change_time[2] = turn_time(u, u->next, v);
        }
        else
        {
            change_time[2] = INF;
        }
        if(u->prev != NULL && u != NULL && v != NULL)
        {
            change_time[3] = turn_time(u->prev, u, v);
        }
        else
        {
            change_time[3] = INF;
        }


        if(u != NULL && v->prev != NULL && v != NULL)
        {
            change_time[4] = turn_time(u, v->prev, v);
        }
        else
        {
            change_time[4] = INF;
        }
        if(u != NULL && v != NULL && v->next != NULL)
        {
            change_time[5] = turn_time(u, v, v->next);
        }
        else
        {
            change_time[5] = INF;
        }


        next_card_time = INF;
        next_card_i = -1;
        for(size_t i = 0; i < 6; ++i)
        {
            if(change_time[i] < next_card_time && change_time[i] > now)
            {
                next_card_i = i;
                next_card_time = change_time[i];
            }
        }
        if(next_card_time == INF)
        {
            break;
        }
        switch (next_card_i)
        {
        case 0:
            p_l++;
            if(from_left->x < u->x)
            {
                ans.push_back(from_left);
            }
            from_left->is_insert();
            break;


        case 1:
            p_r++;
            if(from_right->x > v->x)
            {
                ans.push_back(from_right);
            }
            from_right->is_insert();
            break;


        case 2:
            u = u->next;
            ans.push_back(u);
            break;


        case 3:
            ans.push_back(u);
            u = u->prev;
            break;


        case 4:
            v = v->prev;
            ans.push_back(v);
            break;


        case 5:
            ans.push_back(v);
            v = v-> next;
            break;


        default:
            break;
        }
        now = next_card_time;
    }
    v->prev = u;
    u->next = v;
    for(int i = ans.size() - 1; i >= 0; --i)
    {
        if(ans[i]->x <= u->x || ans[i]->x >= v->x)
        {
            if(ans[i] == u)
            {
                u = u->prev;
            }
            if(ans[i] == v)
            {
                v = v->next;
            }
            ans[i]->is_insert();
        }
        else
        {
            v->prev = ans[i];
            u->next = ans[i];
            ans[i]->prev = u;
            ans[i]->next = v;
            if(ans[i]->x <= all[mid - 1].x)
            {
                u = ans[i];
            }
            else
            {
                v = ans[i];
            }
        }
    }
    return ans;
}
std::vector<Ordered_face> build_3d_hull_down(std::vector<Point> all_point, const long double& INF)
{
    std::sort(all_point.begin(), all_point.end());
    std::vector<Point*> ans_d = build_hull_down(all_point, 0, all_point.size(), INF);
    std::vector<Ordered_face> ans(ans_d.size());
    Ordered_face to_add;
    for(size_t i = 0; i < ans_d.size(); ++i)
    {
        to_add = Ordered_face(ans_d[i]->prev->num, ans_d[i]->num, ans_d[i]->next->num, false, ans_d[i]->is_insert());
        ans[i] = to_add;
    }
    return ans;
}

int main()
{
    const long double INF = 1e18;
    std::vector<Point> all_point;
    long double x, y;
    while(std::cin >> x >> y)
    {
        Point cur;
        cur.x = x;
        cur.y = y;
        cur.z = x * x + y * y;
        cur.next = NULL;
        cur.prev = NULL;
        cur.num = all_point.size();
        all_point.push_back(cur);
    }
    std::vector<Ordered_face> hull_3d_down = build_3d_hull_down(all_point, INF);
    std::map<std::pair<size_t, size_t>, size_t> edges;
    std::vector<bool> in_ans(all_point.size(), true);
    std::vector<size_t> degree(all_point.size(), 0);
    for(size_t i = 0; i < hull_3d_down.size(); ++i)
    {
        Ordered_face cur = hull_3d_down[i];
        edges[{cur.a, cur.b}]++;
        edges[{cur.b, cur.a}]++;

        edges[{cur.a, cur.c}]++;
        edges[{cur.c, cur.a}]++;

        edges[{cur.b, cur.c}]++;
        edges[{cur.c, cur.b}]++;

        degree[cur.a]++;
        degree[cur.b]++;
        degree[cur.c]++;
    }
    for(std::pair<const std::pair<size_t, size_t>, size_t>& cur_edge: edges)
    {
        if(cur_edge.second < 2)
        {
            in_ans[cur_edge.first.first] = false;
            in_ans[cur_edge.first.second]= false;
        }
    }
    long double sum_degree = 0;
    long double sum_am = 0;
    for(size_t i = 0; i < all_point.size(); ++i)
    {
        if(in_ans[i])
        {
            sum_am++;
            sum_degree = sum_degree + degree[i];
        }
    }
    std::cout.precision(7);
    if(sum_am == 0)
    {
        std::cout << std::fixed << 0.0;
    }
    else
    {
        std::cout << std::fixed << sum_degree / sum_am;
    }
    return 0;
}
